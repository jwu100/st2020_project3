# -*- coding: utf-8 -*
import os  

# [Content] XML Footer Text
def test_hasHomePageNode():
	os.system('adb shell uiautomator dump && adb pull /sdcard/window_dump.xml .')
	f = open('window_dump.xml', 'r', encoding="utf-8")
	xmlString = f.read()
	assert xmlString.find('首頁') != -1

# [Behavior] Tap the coordinate on the screen
def test_tapSidebar():
	os.system('adb shell input tap 100 100')

# 1. [Content] Side Bar Text

# 2. [Screenshot] Side Bar Text

# 3. [Context] Categories

# 4. [Screenshot] Categories

# 5. [Context] Categories page

# 6. [Screenshot] Categories page

# 7. [Behavior] Search item “switch”

# 8. [Behavior] Follow an item and it should be add to the list

# 9. [Behavior] Navigate tto the detail of item

# 10. [Screenshot] Disconnetion Screen
